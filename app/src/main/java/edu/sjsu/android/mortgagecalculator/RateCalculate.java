package edu.sjsu.android.mortgagecalculator;

import java.lang.Math;

public class RateCalculate {
    public static float rateCalWithoutJ(float P, float N, float T) {
        return ((P/N) + T);
    }

    public static double rateCalWithJ(float P, float N, float T, float J) {
        float power = N * -1;
        float monthRate = J / 1200;
        double ratio = Math.pow((1 + monthRate), power);
        double jRate = (monthRate/(1 - ratio));
        return ((P * jRate) + T);
    }
}
