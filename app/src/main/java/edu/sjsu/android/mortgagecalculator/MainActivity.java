package edu.sjsu.android.mortgagecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText text;
    private TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editTextTextPersonName);
        resultText = (TextView) findViewById(R.id.textView);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.button:
                float T;

                RadioButton year15 = (RadioButton) findViewById(R.id.radioButton3);
                RadioButton year20 = (RadioButton) findViewById(R.id.radioButton2);
                RadioButton year30 = (RadioButton) findViewById(R.id.radioButton);
                CheckBox myCheck = (CheckBox) findViewById(R.id.checkBox);
                SeekBar bar = (SeekBar) findViewById(R.id.seekBar);
                //To show the current progress of the seekbar
                //reference: https://abhiandroid.com/ui/seekbar
                bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    int progressChangedValue = 0;

                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progressChangedValue = progress;
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                        Toast.makeText(MainActivity.this, "Seek bar progress is :" + (float)progressChangedValue/10,
                                Toast.LENGTH_SHORT).show();
                    }
                });

                try{
                    float inputValue = Float.parseFloat(text.getText().toString());
                }catch(Exception E){
                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }

                float ammountBorrowed = Float.parseFloat(text.getText().toString());
                float interestRate = bar.getProgress()/10;

                if(myCheck.isChecked()){
                    T = ammountBorrowed/1000;
                }else{
                    T = 0;
                }

                if(interestRate == 0){

                    if(year15.isChecked()){
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithoutJ(ammountBorrowed, 15 * 12, T)));
                        year15.setChecked(true);
                        year20.setChecked(false);
                        year30.setChecked(false);
                    } else if(year20.isChecked()){
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithoutJ(ammountBorrowed, 20 * 12, T)));
                        year15.setChecked(false);
                        year20.setChecked(true);
                        year30.setChecked(false);
                    } else{
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithoutJ(ammountBorrowed, 30 * 12, T)));
                        year15.setChecked(false);
                        year20.setChecked(false);
                        year30.setChecked(true);
                    }
                }else{
                    if(year15.isChecked()){
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithJ(ammountBorrowed, 15 * 12, T, interestRate)));
                        year15.setChecked(true);
                        year20.setChecked(false);
                        year30.setChecked(false);
                    } else if(year20.isChecked()){
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithJ(ammountBorrowed, 20 * 12, T, interestRate)));
                        year15.setChecked(false);
                        year20.setChecked(true);
                        year30.setChecked(false);
                    } else{
                        resultText.setText(String.valueOf(RateCalculate.rateCalWithJ(ammountBorrowed, 30 * 12, T, interestRate)));
                        year15.setChecked(false);
                        year20.setChecked(false);
                        year30.setChecked(true);
                    }
                }

                break;
        }
    }

}